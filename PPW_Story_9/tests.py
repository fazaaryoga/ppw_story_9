from django.test import TestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from .views import *
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.

class PPW_Story_9_UnitTest(TestCase):
    
    def testIndexPageExist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def testIndexPageUseIndexHTML(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')

    def testIndexPageUsesIndexHTML(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class PPW_Story_9_FunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        self.options = webdriver.ChromeOptions()
        self.options.add_argument('--dns-prefetch-disable')
        self.options.add_argument('--no-sandbox')
        self.options.add_argument('--headless')
        self.options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=self.options, executable_path='chromedriver')
        self.browser.get(self.live_server_url)
        time.sleep(2)

    def testIndexPageHasTitle(self):
        browser = self.browser
        self.assertIn('', self.browser.title)

    def testSearchBook(self):
        browser = self.browser

        search_box = browser.find_element_by_id("search")
        search_box.send_keys("harry potter")
        search = browser.find_element_by_id("button")
        search.send_keys(Keys.RETURN)

    

    