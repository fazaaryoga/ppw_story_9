from django.urls import path
from . import views

app_name = 'PPW_Story_9'

urlpatterns = [
    path('', views.index, name='index'),
]