console.log('scripts runs')

searchcount = ""
function search() {
    search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    console.log(search)

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",

        success: function (data) {
            console.log('found ' + data.items.length + ' results')
            console.log(data)
            searchcount = data.items.length
            results.innerHTML += "<tr><th id='authors'>Author(s)</th><th id='titles'>Title</th></tr>";
            for (i = 0; i < data.items.length; i++) {
                if (typeof data.items[i].volumeInfo.authors != 'undefined') {
                    results.innerHTML += "<tr><td class='tdata'>" + data.items[i].volumeInfo.authors[0] + "</td><td class='tdata'><p id='A" + [i] + "'>" + data.items[i].volumeInfo.title + "</p></td><td class='tdata'><p id='like" + [i] + "'>0</p></td><td><button class='likebutton' type='button' onclick=like('like" + [i] + "')>Like</button></td></tr>"
                }
                else {
                    results.innerHTML += "<tr><td class='tdata'>No author</td><td class='tdata'><p id='A" + [i] + "'>" + data.items[i].volumeInfo.title + "</p></td><td class='tdata'><p id='like" + [i] + "'>0</p></td><td><button class='likebutton' type='button' onclick=like('like" + [i] + "')>Like</button></td></tr>"
                }
            }
        },

        type: 'GET'
    });
}

function like(e) {
    var like = parseInt(document.getElementById(e).textContent)
    like += 1
    document.getElementById(e).innerHTML = like
}


function display() {
    console.log('function used')
    var top5Likes = []

    for(i=0; i< searchcount; i++){
        top5Likes.push(i)
    }
    console.log(top5Likes)
    console.log('indexed')

    for(i=0;i<top5Likes.length-1;i++){
        var max_ind = i
        //console.log(document.getElementById('like'+i).textContent)
        for(j=i+1; j<top5Likes.length;j++){
            //console.log(document.getElementById('like'+j).textContent)
            if(document.getElementById('like'+top5Likes[j]).textContent > document.getElementById('like'+top5Likes[max_ind]).textContent){
               max_ind = j
            }
        }
        var temp = top5Likes[max_ind]
        top5Likes[max_ind] = top5Likes[i]
        top5Likes[i] = temp
    }
    console.log(top5Likes)
    var modal = document.getElementById('modal')
    var modal_content = document.getElementById('modal_content')
    modal_content.innerHTML = "<h3>Top 5 Most Liked Books</h3>"

    for(i=0;i<5;i++){
        console.log(document.getElementById('A'+i))
        modal_content.innerHTML += "<p>"+ document.getElementById('A'+top5Likes[i]).textContent +"</p>"
    }

    modal.style.display = "block"
    
}   

window.onclick = function(event){
    if(event.target == modal){
        modal.style.display = "none"
    }
}

document.getElementById('favorites').addEventListener('click', display, false)
document.getElementById('button').addEventListener('click', search, false)